package com.minimax.minimax.infrastructure;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class apiController {

    @Value( "${version:1.0.0}" )
    private String version;

    @GetMapping
    public String version() {
        return "version: "+version;
    }

}


