FROM openjdk:17-jdk-alpine as base

WORKDIR /app

COPY .mvn/ .mvn
COPY mvnw pom.xml ./
RUN ./mvnw dependency:go-offline
COPY src ./src

FROM base as test
CMD ["./mvnw", "test"]

FROM base as build
RUN ./mvnw package

FROM eclipse-temurin:17-jre-alpine as dev
EXPOSE 8080

COPY --from=build /app/target/*.jar /connect4.jar

CMD ["java", "-jar", "/connect4.jar"]

FROM eclipse-temurin:17-jre-alpine as prod

COPY --from=build /app/target/*.jar /connect4.jar

CMD ["sh", "-c", "java -Dserver.port=$PORT -jar /connect4.jar"]